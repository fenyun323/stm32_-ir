/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "tim.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
uint8_t send_Flag = 0;
uint32_t send_Code = 0;
uint32_t receive_Code = 0;
uint8_t receive[33] = {0};
uint8_t receive_Flag = 0;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */
void delay_us(uint16_t us){
	uint16_t differ = 0xffff-us-5;				
	__HAL_TIM_SET_COUNTER(&htim1,differ);	//设定TIM定数器的起始值
	HAL_TIM_Base_Start(&htim1);		//启动定时器
	
	while(differ < 0xffff-5){	//判断
		differ = __HAL_TIM_GET_COUNTER(&htim1);		//查询定时器的计数值
	}
	HAL_TIM_Base_Stop(&htim1);
}
//void senddelay_us(uint16_t us){
//	uint16_t differ = 0xffff-us-5;				
//	__HAL_TIM_SET_COUNTER(&htim4,differ);	//设定TIM定数器的起始值
//	HAL_TIM_Base_Start(&htim4);		//启动定时器
//	
//	while(differ < 0xffff-5){	//判断
//		differ = __HAL_TIM_GET_COUNTER(&htim4);		//查询定时器的计数值
//	}
//	HAL_TIM_Base_Stop(&htim4);
//}
uint8_t IR_HighLevelPeriod(void){	//计算红外高电平持续时间
	uint8_t t=0;
	while(HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_4)==1){  //高电平
		t++;
		delay_us(20);
		if(t>=250) return t;	//超时则溢出
	}
	return t;
}
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_TIM2_Init();
  MX_TIM1_Init();
  MX_TIM3_Init();
  /* USER CODE BEGIN 2 */
	TIM2->ARR = 1894-1;
	TIM2->CCR1 = TIM2->ARR/2;
	TIM3->ARR = 199999;
//	HAL_TIM_Base_Start_IT(&htim3);
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
		
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
		if(send_Flag == 1){
			send_Flag = 0;
			HAL_TIM_PWM_Start(&htim2,TIM_CHANNEL_1);
			delay_us(9000);
			HAL_TIM_PWM_Stop(&htim2,TIM_CHANNEL_1);
			delay_us(4500);
			uint8_t addr = 0x58;
			uint8_t data = 0x01;
			send_Code = addr<<24 | (~addr)<<16 | data<<8 | (~data);
			for(int i=31;i>0;i--){
				HAL_TIM_PWM_Start(&htim2,TIM_CHANNEL_1);
				delay_us(560);
				HAL_TIM_PWM_Stop(&htim2,TIM_CHANNEL_1);
				if((send_Code>>i) & 0x01)
					delay_us(560);
				else
					delay_us(1690);
			}
		}
		HAL_GPIO_TogglePin(GPIOC,GPIO_PIN_13);
		HAL_Delay(100);
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin){		//红外遥控外部中断回调函数
	uint8_t Tim=0,Ok=0,Data=0,Num=0;
	while(1){
		if(HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_4)==1){//上升沿判断
			Tim = IR_HighLevelPeriod();	//获得高电平时间

			if(Tim>=250) //5ms
				break;		//无用信号
			if(Tim>=200 && Tim<250)//4.5ms
				Ok=1;		//起始信号
			else if(Tim>=60 && Tim<90)//1.68ms
				Data=1;		//收到数据1
			else if(Tim>=10 && Tim<50)//0.56ms(560us)
				Data=0;		//收到数据0

			if(Ok==1){
				receive_Code <<= 1;
				receive_Code += Data;
				receive[Num] = Data;
				if(Num>=32){
					receive_Flag=1;
						break;
				}
			}
			Num++;
		}
	}
}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
